# Install
Instructions for reproducing the results:

## System requirements
To install system packages, e.g. on Debian, do:
```
sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 06F90DE5381BA480
sudo apt-get update
sudo apt-get install python3 virtualenv postgresql python3-psycopg2 libpq-dev git r-recommended libxml2 libxml2-dev
sudo apt-get upgrade python3-setuptools
```

## Install evorepo
This analysis uses the tools provided by [evorepo](https://github.com/alvarosaurus/evorepo).
After cloning this repository, make sure you pulled evorepo, which is included as a submodule:
```
git submodule update --recursive
```
Then install the required Python and R packages:
```
make install
```
(Alternatively, you may want to install the Python packages in a [virtual environment](https://docs.python.org/3/tutorial/venv.html))

## Getting data
__The recommende way of getting the data__
The data set composed of measurements of code and anonymised organisational characteristics
can be downloaded from: https://zenodo.org/record/1183787.
This data set does not contain the code of the repositories analysed.
```
mkdir Data
cd Data
wget -O VEUNDMINT.csv https://zenodo.org/record/1183787/files/VEUNDMINT.csv?download=1
```

__Alternatively, you can get the metrics yourself__

However, this can be __very time-consuming__. If you prefer to download the data set provided,
you can skip this step (skip to "Analyse data").

If you decide to get the data yourself, you will need to create a new (Postgres) database to hold data and metrics about the repository:
```
make createdb
```

Create a ```Data``` directory.
Make sure you have access to the repositories, and clone them:
```
mkdir Data
git clone git@bitbucket.org:dhaase/ve-und-mint.git -b master --single-branch dhaase-ve-und-mint-master
git clone https://gitlab.tubit.tu-berlin.de/stefan.born/VEUNDMINT_TUB_Brueckenkurs.git
```
Now you can add tags, as explained in the [analysis](https://github.com/alvarosaurus/VEUNDMINT_Analysis/blob/master/docs/analysis.pdf).
Make sure you are in the VEUNDMINT_Analysis directory, and do:
```
make tag
```

Data about the repository is stored in the database: branches, files, file versions, edits and commits;
software metrics are collected for each release: presence/absence and checksums of versions of files,
presence/absence of developers in a release team, number of edits and commits per developer.
Developers are identified by unique, non-reversible cryptographic hashes (md5).
```
make metrics
```
Export the metrics to a spreadsheet that can be used to analyse the data.
The spreadsheet is saved by to ```Data/name-of-the-repository.csv```, a report with counts of what was
exported is saved to ```Data/name-of-the-repository_report.txt```.

```
make export
```

### Analyse data
Calling ```make``` without arguments will now build the article. The article is stored in ```docs/article.pdf```.
The individual steps executed by make are detailed below.

#### Individual steps (optional)

Once the data about the repository has been exported,
you can analyse it by computing a matrix of the pairwise distance between branches.
The matrix will be saved by default to ```Data/name-of-the-repository_matrix.RData```.
```
make matrix
```
Now you can estimate a phylogenetic tree from the distance matrix.
The tree object will be saved by default to ```Data/name-of-the-repository_tree.RData```
```
make tree
```

The tree object can now be visualized.
The plotted image of the tree will be saved by default to ```figures/name-of-the-repository_tree_code.eps``` or ```figures/name-of-the-repository_tree_team.eps```.
```
make figures
```

Some insight about the evolution of the project can be gained by examining the distances
between branches of the tree. For details on the analysis used see the article in ```docs/article.pdf```.
```
make tables
```

