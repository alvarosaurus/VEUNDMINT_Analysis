.PHONY: all prepare clean tag figures
default: all

# Read config file
include config.ini

# the name of the article file (without extension)
ARTICLE = article

# Path to where the TUB repository was cloned
TUB_PATH=Data/VEUNDMINT_TUB_Brueckenkurs
# Path to where the KIT repository was cloned
KIT_PATH=Data/dhaase-ve-und-mint-master

# Database to store software metrics
DBNAME=veundmint
# Name of the forked project
FORKNAME=VEUNDMINT

# Path to Python scripts
export PYTHONPATH=$PYTHONPATH:evorepo/src/DataCollection

# build the article and save it to docs/analysis.pdf
all: clean tables figures prepare 
	cd temp && \
	pdflatex $(ARTICLE).tex && \
	bibtex $(ARTICLE) && \
	pdflatex $(ARTICLE) && \
	pdflatex $(ARTICLE).tex
	mv -f temp/$(ARTICLE).pdf docs
	echo created docs/$(ARTICLE).pdf 

docs/appendix.pdf:
	cd temp && \
	pdflatex appendix.tex
	mv -f temp/appendix.pdf docs

# copy all files required by latex to the temp dir
prepare: 
	mkdir -p temp
	cp -r docs/* temp
	-cp -r figures/* temp
	-cp -r tables/* temp
	-cp -r templates/* temp

# cleanup output and temp files
clean:
	-rm -f docs/$(ARTICLE).pdf
	rm -fr temp/*
	-rm -f ${DATADIR}/${FORKNAME}_tree.png
	-rm -f ${DATADIR}/${FORKNAME}_fork.png

## Install required Python and R packages
install:
	cd evorepo/install && \
	sudo pip3 install -r requirements.txt
	sudo Rscript -e 'install.packages(c("ape", "cluster", "rlang", "ggplot2", "phangorn", "R6", "StatMatch", "testthat", "XML", "pander", "xtable", "schoRsch"), repos="https://cran.uni-muenster.de/")'

## Create a postgres user for the current user
## Create database for tub branch (veundmint_tub)
## Create a database for kit branch (kit_master)
createdb:
	-sudo -u postgres createuser -s ${USER} || :
	createdb veundmint
	psql veundmint -f ${DB_CREATE_FILE}
	echo "Database veundmint created"

###############################
#
# Collect data from the cloned repository
#
###############################

# make one tag for each month since 2016-01-01
MONTHLIST=\
2016-06-01 2016-07-01 2016-08-01 2016-09-01 2016-10-01 2016-11-01 2016-12-01 \
2017-01-01 2017-02-01 2017-03-01 2017-04-01 2017-05-01 2017-06-01 2017-07-01 \
2017-08-01 2017-09-01 2017-10-01 2017-11-01 2017-12-01 2018-01-01
tag:
	$(eval PATH := $(abspath ${TUB_PATH}))
	$(eval BRANCH := multilang)
	$(eval REPONAME := TUB)
	$(foreach var, ${MONTHLIST}, $(eval DATE := ${var}) $(tag_cmd);)
	echo "Tagged TUB"

	$(eval PATH := $(abspath ${KIT_PATH}))
	$(eval BRANCH := master)
	$(eval REPONAME := KIT)
	$(foreach var, ${MONTHLIST}, $(eval DATE := ${var}) $(tag_cmd);)
	echo "Tagged KIT"

store:
	$(eval REPONAME := TUB)
	$(eval PATH := $(abspath ${TUB_PATH}))
	$(foreach var, ${MONTHLIST}, $(eval BRANCHNAME := ${REPONAME}-${var}) $(collect_cmd);)
	echo "Repository data for TUB branch stored in database"

	$(eval REPONAME := KIT)
	$(eval PATH := $(abspath ${KIT_PATH}))
	$(foreach var, ${MONTHLIST}, $(eval BRANCHNAME := ${REPONAME}-${var}) $(collect_cmd);)
	echo "Repository data for KIT branch stored in database"

metrics: store
	export PYTHONPATH=${PYTHONPATH}:evorepo/src/DataCollection

	$(eval REPONAME := TUB)
	$(eval PATH := $(abspath ${TUB_PATH}))
	$(eval STARTTAG := '')
	$(foreach var, ${MONTHLIST}, $(eval ENDTAG := ${REPONAME}-${var}) $(metrics_cmd) $(eval STARTTAG := ${REPONAME}-${var});)
	echo "Computed metrics for TUB branch stored in the database"

	$(eval REPONAME := KIT)
	$(eval PATH := $(abspath ${KIT_PATH}))
	$(eval STARTTAG := '')
	$(foreach var, ${MONTHLIST}, $(eval ENDTAG := ${REPONAME}-${var}) $(metrics_cmd) $(eval STARTTAG := ${REPONAME}-${var});)
	echo "Computed metrics for KIT branch stored in the database"	

#########################################
#
# Export data to one spreadsheet
# for each metrics type (code and team).
#
########################################

export:${CSV_CODE_FILE} ${CSV_TEAM_FILE}
	echo "Data collected was exported to $(abspath ${DATADIR}/${FORKNAME}_code.csv) $(abspath ${DATADIR}/${FORKNAME}_team.csv)."

${CSV_CODE_FILE}:
	$(eval EXPORTTYPE := 2) ## export code measurements
	$(eval SPREADSHEET := $(abspath ${DATADIR}/${FORKNAME}_code.csv))
	$(export_cmd)

${CSV_TEAM_FILE}:
	$(eval EXPORTTYPE := 3) ## export team measurements
	$(eval SPREADSHEET := $(abspath ${DATADIR}/${FORKNAME}_team.csv))
	$(export_cmd)

#########################################
#
# Compute a distance matrix
# for each metrics type (code and team).
#
########################################

matrix:${MATRIX_TEAM_FILE} ${MATRIX_CODE_FILE}
	echo "Saved matrix to ${MATRIX_TEAM_FILE}, ${MATRIX_CODE_FILE}"

${MATRIX_TEAM_FILE}:${CSV_TEAM_FILE}
	$(eval sourcePath:=$(abspath ${CSV_TEAM_FILE}))
	$(eval destFile:=$(abspath ${MATRIX_TEAM_FILE}))
	$(matrix_cmd) 

${MATRIX_CODE_FILE}:${CSV_CODE_FILE}
	$(eval sourcePath:=$(abspath ${CSV_CODE_FILE}))
	$(eval destFile:=$(abspath ${MATRIX_CODE_FILE}))
	$(matrix_cmd) 

#########################################
#
# Estimate the phylogenetic tree
# for each metrics type (code and team).
#
########################################

tree:${TREEOBJ_TEAM_FILE} ${TREEOBJ_CODE_FILE}
	echo "Saved tree object to ${TREEOBJ_TEAM_FILE}, ${TREEOBJ_CODE_FILE}"

${TREEOBJ_TEAM_FILE}:${MATRIX_TEAM_FILE}
	$(eval sourcePath:=$(abspath ${MATRIX_TEAM_FILE}))
	$(eval destFile:=$(abspath ${TREEOBJ_TEAM_FILE}))
	$(treeobj_cmd)

${TREEOBJ_CODE_FILE}:${MATRIX_CODE_FILE}
	$(eval sourcePath:=$(abspath ${MATRIX_CODE_FILE}))
	$(eval destFile:=$(abspath ${TREEOBJ_CODE_FILE}))
	$(treeobj_cmd)

########################
#
# Article Tables
#
########################

tables:tables/${FORKNAME}_analysis_code.tex tables/${FORKNAME}_analysis_team.tex
	echo "Done generating tables"

tables/${FORKNAME}_analysis_code.tex:${TREEOBJ_CODE_FILE}
	$(eval sourcePath:=$(abspath ${TREEOBJ_CODE_FILE}))
	$(eval destFile:=$(abspath ${ANALYSIS_CODE_FILE}))
	$(eval caption:="Pairwise distances of branches vs. forks for the code dataset.")
	$(eval label:="analysis_code")
	$(analysis_cmd)
	mv ${ANALYSIS_CODE_FILE} tables/

tables/${FORKNAME}_analysis_team.tex:${TREEOBJ_TEAM_FILE}
	$(eval sourcePath:=$(abspath ${TREEOBJ_TEAM_FILE}))
	$(eval destFile:=$(abspath ${ANALYSIS_TEAM_FILE}))
	$(eval caption:="Pairwise distances of branches vs. forks for the team dataset.")
	$(eval label:="analysis_team")
	$(analysis_cmd)
	mv ${ANALYSIS_TEAM_FILE} tables/

########################
#
# Figures
#
########################

figures:figures/${FORKNAME}_tree_team.eps figures/${FORKNAME}_tree_code.eps figures/${FORKNAME}_fork_team.eps figures/${FORKNAME}_fork_code.eps
	echo "Done generating figures"

figures/${FORKNAME}_tree_team.eps:${TREEOBJ_TEAM_FILE}
	$(eval sourcePath:=$(abspath ${TREEOBJ_TEAM_FILE}))
	$(eval destFile:=$(abspath ${TREE_TEAM_FILE}))
	$(plot_cmd)
	mv ${TREE_TEAM_FILE} figures/

figures/${FORKNAME}_tree_code.eps:${TREEOBJ_CODE_FILE}
	$(eval sourcePath:=$(abspath ${TREEOBJ_CODE_FILE}))
	$(eval destFile:=$(abspath ${TREE_CODE_FILE}))
	$(plot_cmd)
	mv ${TREE_CODE_FILE} figures/

figures/${FORKNAME}_fork_team.eps:${TREEOBJ_TEAM_FILE}
	$(eval sourcePath:=$(abspath ${TREEOBJ_TEAM_FILE}))
	$(eval destFile:=$(abspath ${FORK_TEAM_FILE}))
	$(fork_cmd)
	mv ${FORK_TEAM_FILE} figures/

figures/${FORKNAME}_fork_code.eps:${TREEOBJ_CODE_FILE}
	$(eval sourcePath:=$(abspath ${TREEOBJ_CODE_FILE}))
	$(eval destFile:=$(abspath ${FORK_CODE_FILE}))
	$(fork_cmd)
	mv ${FORK_CODE_FILE} figures/

########################
#
# Command definitions
#
########################

define tag_cmd
	cd ${PATH} && /usr/bin/git checkout `/usr/bin/git rev-list -n 1 --before="${DATE}" ${BRANCH}` && /usr/bin/git tag ${REPONAME}-${DATE}
endef

define collect_cmd
	cd ${PATH} && /usr/bin/git checkout -f ${BRANCHNAME}; /usr/bin/git clean -fdx
	/usr/bin/python3 evorepo/src/DataCollection/collect.py \
		 ${FORKNAME} ${REPONAME} ${BRANCHNAME} ${PATH} ${DBNAME} 
endef

define metrics_cmd
	/usr/bin/python3 evorepo/src/DataCollection/addmetrics.py \
		${REPONAME} ${STARTTAG} ${ENDTAG} ${PATH} ${DBNAME} 
endef

define export_cmd
	/usr/bin/python3 evorepo/src/DataCollection/export.py \
		${DBNAME} ${FORKNAME} ${SPREADSHEET} ${EXPORTTYPE}
endef

define matrix_cmd
	cd evorepo && /usr/bin/Rscript src/DataAnalysis/distance_matrix.R ${sourcePath} ${destFile}
endef

define treeobj_cmd
	cd evorepo && /usr/bin/Rscript src/DataAnalysis/tree.R ${sourcePath} ${destFile} ${TREE_ROOT} ${TREE_FILTER}
endef

define plot_cmd
	cd evorepo && /usr/bin/Rscript src/DataAnalysis/plot_eps.R ${sourcePath} ${destFile}
endef

define analysis_cmd
	cd evorepo && /usr/bin/Rscript src/DataAnalysis/analysis.R ${sourcePath} ${destFile} ${caption} ${label}
        ## remove garbage in the destination file
	sed -i s/\\[1\\]// ${destFile}
	sed -i s/'\\\\'/'\\'/ ${destFile}
endef

define fork_cmd
	cd evorepo && /usr/bin/Rscript src/DataAnalysis/fork_eps.R ${sourcePath} ${destFile}
endef
