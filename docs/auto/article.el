(TeX-add-style-hook
 "article"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-class-options
                     '(("wlpeerj" "fleqn" "10pt" "lineno")))
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("fontenc" "T1")))
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "path")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "url")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "nolinkurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperbaseurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperimage")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperref")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "path")
   (TeX-run-style-hooks
    "latex2e"
    "data_characteristics"
    "VEUNDMINT_analysis_code"
    "VEUNDMINT_analysis_team"
    "wlpeerj"
    "wlpeerj10"
    "tabu"
    "float"
    "hyperref"
    "fontenc")
   (TeX-add-symbols
    '("e" 1)
    '("dd" 1)
    '("dt" 1)
    '("definition" 2))
   (LaTeX-add-labels
    "fig:tree_code"
    "fig:tree_team")
   (LaTeX-add-bibliographies
    "analysis"))
 :latex)

