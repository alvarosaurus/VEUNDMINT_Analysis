(TeX-add-style-hook
 "appendix"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("geometry" "a4paper" "margin=1cm" "landscape") ("inputenc" "utf8") ("babel" "english")))
   (TeX-run-style-hooks
    "latex2e"
    "cophenetic"
    "article"
    "art10"
    "geometry"
    "inputenc"
    "babel"
    "tabu"
    "float"
    "lscape"))
 :latex)

