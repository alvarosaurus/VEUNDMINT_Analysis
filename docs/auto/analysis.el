(TeX-add-style-hook
 "analysis"
 (lambda ()
   (TeX-run-style-hooks
    "preamble"
    "data_characteristics"
    "VEUNDMINT_analysis_code"
    "VEUNDMINT_analysis_team")
   (LaTeX-add-labels
    "fig:tree_code"
    "fig:tree_team")
   (LaTeX-add-bibliographies))
 :latex)

