(TeX-add-style-hook
 "preamble"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-class-options
                     '(("article" "letterpaper" "12pt")))
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("inputenc" "utf8") ("mathpazo" "sc") ("fontenc" "T1") ("babel" "english") ("geometry" "top=2.5cm" "bottom=2.5cm" "left=2.5cm" "right=2.5cm") ("caption" "hang" "small" "labelfont=bf" "up" "textfont=it")))
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "path")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "url")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "nolinkurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperbaseurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperimage")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperref")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "path")
   (TeX-run-style-hooks
    "latex2e"
    "article"
    "art12"
    "times"
    "blindtext"
    "inputenc"
    "mathpazo"
    "fontenc"
    "microtype"
    "babel"
    "geometry"
    "caption"
    "booktabs"
    "lettrine"
    "enumitem"
    "abstract"
    "titlesec"
    "fancyhdr"
    "titling"
    "hyperref"
    "tabu"
    "siunitx"
    "float"
    "natbib"
    "graphicx"
    "lscape"
    "lineno")
   (TeX-add-symbols
    '("e" 1)
    '("dd" 1)
    '("dt" 1)
    '("definition" 2)))
 :latex)

