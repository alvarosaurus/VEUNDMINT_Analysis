\input{preamble}

\begin{document}

% Print the title
\begin{titlepage}
\maketitle
\textsuperscript{1}Museum für Naturkunde Berlin \\

Corresponding author:\\

Alvaro Ortiz-Troncoso\textsuperscript{1} \\

Email address: \href{mailto:alvaro.OrtizTroncoso@mfn.berlin}{alvaro.OrtizTroncoso@mfn.berlin}
\end{titlepage}
%----------------------------------------------------------------------------------------
%	ARTICLE CONTENTS
%----------------------------------------------------------------------------------------
\begin{abstract}
  \noindent
  Open source projects may face a forking situation at some point during their life-cycle. The traditional view is that forks are a waste of project resources and should be avoided. However, in a wider technological and organisational context, forks can be a way to foster the creation of a software ecosystem. Either way, forking is explicitly allowed by open source licenses. Notwithstanding, methods for quantifying the evolution of forks are currently scarce. The present work attempts to answer the question whether a real-life project has forked. It does so by considering code and organisational characteristics of the project, and analysing these characteristics by applying methods ported from biological phylogenetics. After finding that the project is forked, implications for project governance are discussed.
\end{abstract}

\section{Introduction}

\lettrine[nindent=0em,lines=3]{I}n open source software development, a fork is a bifurcation from an existing project, resulting in an autonomous development strand, with its own name, infrastructure, code base, and community of developers \citep{Robles2012a}. Organisations adopting open source software development might face a fork situation during the software’s life-cycle, or might voluntarily fork existing software as a means to solve technical-, license-, and team-related issues \citep{Nyman2013a, Robles2012a}.

Knowing whether a project will likely fork (or has forked already) can therefore facilitate project management. Phylogenetic methods, ported from evolutionary biology, can be used to estimate a phylogenetic tree of a project \citep{OrtizTroncoso2018a}, and hence provide a method for estimating whether two diverging development strands are more likely to fork than to merge. The present work applies phylogenetic methods to the evolution of a real-world project, VE\&MINT.

\medskip
The VE\&MINT project is a cooperation between several German institutes: MINT-Kolleg Baden-Württemberg with the VEMINT-Konsortium, the Leibniz Universität Hannover, and the Technische Universität Berlin \citep{VEMINT}. The project's purpose is to offer a preparatory mathematics course online\footnote{The version of the course developed at the Technische Universität Berlin can be accessed at: \url{https://www.math4refugees.tu-berlin.de/}}. Technically, course modules are written using the document preparation system LaTeX. The modules are transformed into formats suitable for online presentation using a software originally authored at the Karlsruhe Institute of Technology (KIT) and further developed at the Technische Universität Berlin (TUB). There are two main software development strands: the KIT and the TUB strands. These strands are developed by different teams, yet the name of the project is the same, and the teams have access to each other's code repositories. Therefore, the definition of fork proposed by \citet{Robles2012a}, introduced above, applies only in part. The purpose of the present analysis is to study the evolution of the VE\&MINT project by answering the following question: 

\definition{Research Question}{Can the KIT and TUB software development strands be considered parts of one project or is the original project forked in two different projects?}

The answer to this question could be instrumental in directing the development effort in future iterations of the project.

% ------------------------------------------------

\section{Materials and Methods}
The ``master'' branch, maintained by the Karlsruhe Institute of Technology (KIT) and the ``multilang'' branch, maintained by the Technische Universität Berlin (TUB) were used for the analysis, as these are the two branches under active development.

The two development strands do not have a common release scheme. In order to quantify development, it was necessary to create releases. This was accomplished by creating a release for each month in each branch. As TUB development started in June 2016, the chosen time period spans from the 1\textsuperscript{st} July 2016 to the 1\textsuperscript{st} January 2018.

Software metrics were computed for each month in each branch. The choice of software metrics is based, with alterations, on the overview of software metrics provided by \citet{nagappan2008influence}. Metrics about team members were anonymised. A summary of the metrics retained and of their data types is given in table \ref{table:data_characteristics}.

%-- table ----------
\input{data_characteristics.tex}

A matrix of pairwise dissimilarity between releases was computed. As the measurements used are of different data types (table \ref{table:data_characteristics}), the Gower algorithm for computing distance matrices was used, since this algorithm can combine measurements of mixed data types \citep{DOrazio2016}.

The Neighbour-Joining (NJ) algorithm \citep{Saitou1987a} was used to estimate a phylogenetic tree from the distance matrix, as previous work on well-documented cases of forks, e.g., the MySQL / MariaDB fork \citep{OrtizTroncoso2018a}, suggests that the NJ algorithm produces the most accurate results when applied to the evolution of software. The tree was rooted at the last release prior to the start of development at the TUB (June 2016).

A cophenetic distance matrix was computed to obtain a numerical representation of the phylogenetic tree: The elements of the cophenetic distance matrix are the pairwise distances between the tips of the tree \citep[p. 1275]{RDevelopmentCoreTeam2008a}. The correlation between the distance matrix (obtained from code and team characteristics) and the cophenetic distance matrix (obtained from the tree) was used as a measurement of how well the tree represents the data.

\bigskip
Whether the VE\&MINT project is forked was answered by performing a comparison of the mean cophenetic distance between releases in the same development strand (``branches'') vs. across strands (``forks''), (building upon \citet{OrtizTroncoso2018a}). The research question can be expressed as a statistical hypothesis:

\definition{Statistical hypothesis}{A project is forked when a branch can be told from a fork by examining the pairwise distance between releases: if the mean cophenetic distance between releases is significantly different (p < 0.05) and larger depending on whether the releases are on the same or on different development strands, then the project is forked.}

\noindent
The research question can thus be answered by comparing the means of the pairwise cophenetic distances for two treatments (``branches'' and ``forks''). Two variables are examined:
\begin{itemize}
\item{One nominal variable: whether a pair of releases are on the same development strand (``branches'') or on different development strands (``forks'').}
\item{One measurement variable: the pairwise cophenetic distance between releases.}
\end{itemize}

 As the variance is not assumed to be equal for the two treatments, the Welch two-sample t-test was chosen \citep[p. 130]{McDonald2014b}. A t-test assumes that the data is normally distributed. Performing a t-test on data that is not normally distributed increases the risk of false positives \citep[p. 128]{McDonald2014b}. Previous work has shown that the distribution of cophenetic distances between software releases is not normally distributed, but that the square root of distances better fits the normal distribution \citep{OrtizTroncoso2018a}. Therefore, the t-test was performed on square root transformed data.

The data set composed of measurements of code and anonymized organisational characteristics can be downloaded from: \url{http://doi.org/10.5281/zenodo.1327424}. This data set does not contain the code of the repositories analysed.

The analysis was done using Evorepo, a software for constructing phylogenetic trees of software evolution \citep{alvaro_ortiz_troncoso_2018_1327427}. The release used to produce the phylogenetic analysis for this work, can be downloaded from: \url{http://doi.org/10.5281/zenodo.1327427}.

%------------------------------------------------

\section{Results}
The measurements obtained from the releases were stored in a database and exported to a spreadsheet consisting of one column for each release and one row for each measurement. Two data sets were created: one for the measurements obtained from code characteristics and one for the measurements obtained from team characteristics (table \ref{table:data_characteristics}). A matrix of pairwise distances between releases was computed based on each data set, and a phylogenetic tree was estimated from the distance matrix obtained from each data set, applying the methods described above (figures \ref{fig:tree_code} and \ref{fig:tree_team}). The trees represents the distance matrix accurately (code data: 0.9999605 correlation, p < 2.2\e{-16}; team data: 0.9843978 correlation, p < 2.2\e{-16}).

\begin{figure}[H]
  \centering
  \includegraphics[width=1\textwidth]{VEUNDMINT_tree_code.eps}
  \caption{A phylogenetic tree of the VE\&MINT project, obtained from \textbf{code} characteristics.}
  \label{fig:tree_code}
\end{figure}

\begin{figure}[H]
  \centering
  \includegraphics[width=1\textwidth]{VEUNDMINT_tree_team.eps}
  \caption{A phylogenetic tree of the VE\&MINT project, obtained from \textbf{team} characteristics.}
  \label{fig:tree_team}
\end{figure}

In order to answer the research question, a correlation was sought between the main pairwise distance between releases and whether these releases are part of the same development strand (``branches'') or are part of different strands (``forks''). The analysis was repeated for the data obtained from code characteristics and for the data obtained from team characteristics (tables \ref{table:analysis_code} and \ref{table:analysis_team}). The analysis shows that the the null hypothesis can be rejected (p < 2\e{-16}) for each data set. Therefore, the distance between releases on the same strand (``branches'') is significantly different from the distance between releases across strands (``forks'').

%-- table ----------
\input{VEUNDMINT_analysis_code.tex}
\input{VEUNDMINT_analysis_team.tex}

%------------------------------------------------

\section{Discussion}

The estimated phylogenetic trees (figures \ref{fig:tree_code} and \ref{fig:tree_team}) show all releases as a monophyletic clade, i.e., they all descend from a common ancestor. The most recent common ancestor of all releases is the KIT release dated June 2016. After that, the algorithm estimated that the most parsimonious tree, i.e., the simplest and therefore the most likely tree, is a tree where most KIT and TUB releases are grouped separately.

The analysis of variance found that the distance between releases of different development strands is significantly larger than the distance inside development strands. This provides evidence for the hypothesis that the project could be said to be forked.
\medskip \\
\citet{Nyman2013a} discuss the implications of forking from the point of view of project governance. They argue that forking affects open source project at three levels:
\begin{itemize}
\item{At the software level, a fork creates a redundancy, reduces the probability that a project might become obsolete, and hence increases the lifespan of a project.}
\item{At the community level, a fork allows to tackle multiple issues, using different technologies, and hence increases the sustainability of a project's community of developers.}
  \item{At the business level, forks provide a mechanism for quickly responding to changing needs in the user base.}
\end{itemize}
On the other hand, forking can result in competing products, i.e. competing for the participation of developers. Therefore, in order to take advantage of the possibilities offered by a fork, the governance of a forked project should focus on nurturing a community of practice (encompassing forked strands) and encouraging external participation \cite{Kogut2001a}.

%------------------------------------------------

\section{Conclusions}
A phylogenetic analysis of the development of the VE\&MINT project found evidence that the development strands at the KIT and at the TUB are forked.

The phylogenetic analysis is neither capable of discerning the reasons for the fork nor to make predictions about the fork's outcome \citep{OrtizTroncoso2018a}. However, having obtained quantitative evidence that the project is forked could facilitate a discussion on the way the project should be steered, as well as on the validity of the method presented here.

\section{Acknowledgements}
I wish to thank Erhard Zorn of TUB for providing feedback on the first draft of this paper.

% ----------------------------------------------------------------------------------------
%	BIBLIOGRAPHY
%----------------------------------------------------------------------------------------

\addcontentsline{toc}{chapter}{Bibliography}
\bibliographystyle{abbrvnat}
\bibliography{analysis}

%----------------------------------------------------------------------------------------

\end{document}
