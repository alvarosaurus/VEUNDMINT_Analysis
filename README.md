# VEUNDMINT_Analysis
Phylogenetic analysis of software evolution, applied to a real-life project. 

This is a work-in-progress. The latest version is in
[docs/article.pdf](https://gitlab.com/alvarosaurus/VEUNDMINT_Analysis/blob/master/docs/article.pdf).

Data: [![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.1327424.svg)](https://doi.org/10.5281/zenodo.1327424)
Software and LaTeX source: [![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.1401151.svg)](https://doi.org/10.5281/zenodo.1401151)

If you want to reproduce the results, please see [INSTALL.md](INSTALL.md).


