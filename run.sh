#! /bin/bash

# Read configuration options
source config.ini

usage() {
	echo "See https://github.com/alvarosaurus/evorepo."
}

#matrix() {
#	sourcePath=$(realpath ${CSV_TEAM_FILE})
#	destFile=$(realpath ${MATRIX_TEAM_FILE})
#	cd evorepo && /usr/bin/Rscript src/DataAnalysis/distance_matrix.R ${sourcePath} ${destFile}
#       
#	sourcePath=$(realpath ${CSV_CODE_FILE})
#	destFile=$(realpath ${MATRIX_CODE_FILE})
#	cd evorepo && /usr/bin/Rscript src/DataAnalysis/distance_matrix.R ${sourcePath} ${destFile}
#
#       echo "Saved distance matrix to file ${MATRIX_FILE}"
#}

#tree() {
#	sourcePath=$(realpath ${MATRIX_FILE})
#	destFile=$(realpath ${TREEOBJ_FILE})
#	cd evorepo && /usr/bin/Rscript src/DataAnalysis/tree.R ${sourcePath} ${destFile} ${TREE_ROOT} ${TREE_FILTER}
#
#        echo "Saved tree object to file ${TREEOBJ_FILE}"
#}

#plot() {
#	sourcePath=$(realpath ${TREEOBJ_FILE})
#	destFile=$(realpath ${TREE_FILE})
#	cd evorepo && /usr/bin/Rscript src/DataAnalysis/plot_eps.R ${sourcePath} ${destFile}
#
#       echo "Plotted tree to file ${destFile}"
#}

histo() {
	sourcePath=$(realpath ${TREEOBJ_FILE})
	destFile=$(realpath ${HISTO_FILE})
	cd evorepo && /usr/bin/Rscript src/DataAnalysis/histo.R ${sourcePath} ${destFile}

        echo "Plotted histogram to file ${HISTO_FILE}"
}

cophenetic_code() {
        ## pass the matrix, as the tree needs to be estimated without dropping any tips (e.g. root)
	matrixPath=$(realpath ${MATRIX_CODE_FILE})
        ##	destFile=$(realpath ${COPH_CODE_FILE})
        
	cd evorepo && /usr/bin/Rscript src/DataAnalysis/cophenetic.R ${matrixPath} # ${destFile}
##        echo "saved cophenetic matrix to file ${COPH_FILE} as LaTeX table"
}

cophenetic_team() {
	matrixPath=$(realpath ${MATRIX_TEAM_FILE})
##	destFile=$(realpath ${COPH_CODE_FILE})
	cd evorepo && /usr/bin/Rscript src/DataAnalysis/cophenetic.R ${matrixPath} # ${destFile}
##        echo "saved cophenetic matrix to file ${COPH_FILE} as LaTeX table"
}

progression() {
	sourcePath=$(realpath ${TREEOBJ_FILE})
	destFile=$(realpath ${PROGRESSION_FILE})
	cd evorepo && /usr/bin/Rscript src/DataAnalysis/progression.R ${sourcePath} ${destFile}

        echo "saved histogram to file ${PROGRESSION_FILE}"
}

fork() {
	sourcePath=$(realpath ${TREEOBJ_FILE})
	destFile=$(realpath ${FORK_FILE})
	cd evorepo && /usr/bin/Rscript src/DataAnalysis/fork_eps.R ${sourcePath} ${destFile}

        echo "saved histogram to file ${destFile}"
}

#analysis() {
#	sourcePath=$(realpath ${TREEOBJ_FILE})
#	destFile=$(realpath ${ANALYSIS_FILE})
#	cd evorepo && /usr/bin/Rscript src/DataAnalysis/analysis.R ${sourcePath} ${destFile}
#       ## remove garbage in the destination file
#	sed -i s/\\[1\\]// ${destFile}
#	sed -i s/'\\\\'/'\\'/ ${destFile}
#}

$1
